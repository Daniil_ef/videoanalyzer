#include <iostream>
#include <opencv2/opencv.hpp>

void homework() {
    Mat image;
    image = imread("pic1.jpg", 1 );

    show_save("Source")
    imshow("Display Image", image);

    waitKey(0);

    Mat dst_Gaussian = image.clone();

    GaussianBlur(image, dst_Gaussian, Size(5, 5), 0, 0);
    imshow("Gausian_filter", dst_Gaussian);

    waitKey(0);

    Mat dst_Median = image.clone();

    medianBlur(image, dst_Median, 5);
    imshow("Median_filter", dst_Median);

    Mat RotatedImage = image.clone();

    Mat rot_mat( 2, 3, CV_32FC1 );

    Point center = Point( image.cols/2, image.rows/2 );
    double angle = -15.0;
    double scale = 1;

    rot_mat = getRotationMatrix2D(center, angle, scale);

    warpAffine(image, RotatedImage, rot_mat, image.size());

    imshow("RotateImage", RotatedImage);

    waitKey(0);
}

void show_save(const char* title, const Mat& image) {
    imshow(title, image);
    imwrite(title, image);

    waitkey(0);
}
