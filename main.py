# -*- coding: utf-8 -*-
import os

import cv2
import numpy as np
import scipy.spatial.distance as dst
from sklearn.cluster import KMeans
from sklearn.neighbors import DistanceMetric

import swlib

evk = DistanceMetric.get_metric('euclidean')
N = 10
match = 3
mismatch = -1
main_file = "stip/karusel/vid_05_4"
adv_file = "stip/karusel/adv_stip_05_4"
gatsby = "stip/Gatsby/stip_01_4"

# Вспомогательные функции


def get_dist(a, b):
    return sum([dst.euclidean(a[i], b[i]) for i in range(len(a))]) / len(a)


def get_time_by_frame(a):
    return str(a[0] / 600) + ":" + str(a[0] % 600 / 10)


def get_seq(file_name):
    with open(file_name, 'r') as f:
        file = f.read().split('\n')
        title = file[0:2]  # Обрезаем информацию от стипа о формате данных
        file = file[2:]

    matrix = []
    infolist = []
    for el in file:
        matrix.append(list(map(float, el.split(' ')[9:-1])))
        infolist.append(el.split(' ')[0:9])

    if matrix[-1] == []:
        matrix = matrix[:-1]
    matrix = np.array(matrix)  # Матрица векторов hog + hof

    # Разбиваем на N кластеров
    kmeans = KMeans(n_clusters=N, precompute_distances=True).fit(matrix)
    centres = kmeans.cluster_centers_
    labels = kmeans.labels_
    matrix = matrix.tolist()

    #  Создаем массив [(time : label)]
    seqcl = {}
    for i in range(len(labels)):
        # seqcl.append([infolist[i][6], labels[i]])
        if int(infolist[i][6]) not in seqcl.keys():
            seqcl[int(infolist[i][6])] = labels[i]
        else:
            dist1 = evk.pairwise([matrix[i], centres[labels[i]]])[0][1]
            dist2 = evk.pairwise([matrix[seqcl[int(infolist[i][6])]], centres[seqcl[int(infolist[i][6])]]])[0][1]
            if dist1 < dist2:
                seqcl[int(infolist[i][6])] = labels[i]

    #  Сортируем по времени
    seq = sorted(seqcl.items(), key=lambda a: a[0])
    return seq


# Основные функции

def find_adv_in_stream():
    #  Открываем файл с рекламой
    seq_item1 = get_seq(adv_file)
    seq1 = list(map(str, [item[1] for item in seq_item1]))

    #  Открываем файл с общий файл
    seq_item2 = get_seq(main_file)
    seq2 = list(map(str, [item[1] for item in seq_item2]))

    lengths = []
    pers = {}
    while True:
        #  Запускаем алгоритм Смита-Ватермана.
        sw = swlib.LocalAlignment(swlib.NucleotideScoringMatrix(match, mismatch)) # Matching 2, mismatching -1
        al = sw.align(seq1, seq2)
        #  Заменяем найденое вхождение рекламы в потоке на ***
        f = open(os.devnull, "w")
        start, length, perc = al.dump(out=f)
        lengths.append(str(length))
        if perc > 0.4 and length > 10:
            pers[start - 1] = perc
            for j in range(start - 1, min(start + length - 1, len(seq2))):
                seq2[j] = '*'
        else:
            break

    # Выводим предварительные начало и конец рекламных роликов
    # И процент совпадения с фрагметом из большого видео
    flag = -1
    intervals = []
    for i in range(len(seq2)):
        if i in pers.keys():
            print "Percent of coincidence: %.2g" % pers[i]
        if seq2[i] == '*' and flag == -1:
            flag = 0
            print "Adv start: ", get_time_by_frame(seq_item2[i])
            intervals.append(seq_item2[i][0])
        elif seq2[i] == '*' and flag == 0:
            continue
        elif flag == 0:
            flag = -1
            print "Adv end: ", get_time_by_frame(seq_item2[i])
            intervals.append(seq_item2[i][0])
            print "\n"
        elif i == len(seq2) - 1 and seq2[i] == "*":
            print "Adv end: ", get_time_by_frame(seq_item2[i])
            intervals.append(seq_item2[i][0])
            print "\n"

    # Получаем поток фреймов для видео, и массив фреймов для рекламы
    def get_frames(file, whole=False):
        adv_frames = cv2.VideoCapture(file)
        if not whole:
            return adv_frames
        else:
            adv_list = []
            while True:
                ret, frame = adv_frames.read()
                if not ret:
                    break
                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                adv_list.append(gray.tolist())
            return adv_list

    # проверяем вхождение кадра в начало
    def check_int(gray, adv_frames, start=True):
        if start:
            return min(get_dist(gray, adv_frames[0]), get_dist(gray, adv_frames[1]))
        else:
            return min(get_dist(gray, adv_frames[-1]), get_dist(gray, adv_frames[-2]))

    # Массив фреймов для видео-рекламы
    adv_frames = get_frames("stip/karusel/adv_10.avi", whole=True)

    # Поток, с которого можно считывать фреймы
    vid_frames = get_frames("stip/karusel/vid_2.avi")

    frame_i = 0
    interval_i = 0
    flag = False
    real_st = 0
    real_nd = 0
    # Проходимся по основному видеофайлу и определяем границы рекламного ролика
    # С помощь сравнения евклидовых расстояний
    # Эмпирическим путём выяснено, что евклидово расстояние между случайными фреймами - >1800,
    # А между похожими - <300
    while True:
        if interval_i + 1 >= len(intervals):
            if real_st > 0 and real_nd > 0:
                print "real interval:", real_st, real_nd
            break
        ret, frame = vid_frames.read()
        if not ret:
            break
        if intervals[interval_i] == frame_i:
            flag = True
            real_st = 0
            real_nd = 0
        if flag:
            if intervals[interval_i + 1] < frame_i:
                if real_st > 0 and real_nd > 0:
                    print "real interval:", real_st, real_nd
                interval_i += 2
                flag = False

            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            dist_st = check_int(gray.tolist(), adv_frames, start=True)
            dist_nd = check_int(gray.tolist(), adv_frames, start=False)
            if dist_st < 300:
                real_st = frame_i
            if dist_nd < 300:
                real_nd = frame_i

        frame_i += 1


def find_new_adv():
    N = 50
    gseq_item1 = get_seq(gatsby)
    gseq = list(map(str, [item[1] for item in gseq_item1]))

    for i in range(2):
        #  Запускаем алгоритм Смита-Ватермана.
        #  Каждый раз с ячеки, у которой очков меньше, чем у предыдущей
        sw = swlib.LocalAlignment(swlib.NucleotideScoringMatrix(match, mismatch), start_num=i) # Matching 2, mismatching -1
        al = sw.align(gseq, gseq)
        #  Заменяем найденое вхождение рекламы в потоке на ***
        f = open(os.devnull, "w")
        start, length, perc = al.dump(out=f)
        if perc > 0.4 and length > 10:
            print "New advertisment:", start, start + length
        else:
            break


if __name__ == "__main__":
    find_adv_in_stream()
    find_new_adv()
